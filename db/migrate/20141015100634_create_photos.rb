class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|
      t.string :pic
      t.string :name
      t.text :description
      t.decimal :price
      t.string :location
      t.timestamp :shoot_time

      t.timestamps
    end
  end
end
