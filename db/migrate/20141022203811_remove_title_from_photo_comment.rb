class RemoveTitleFromPhotoComment < ActiveRecord::Migration
  def change
		remove_column :photo_comments, :title
  end
end
