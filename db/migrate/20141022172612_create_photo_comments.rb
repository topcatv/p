class CreatePhotoComments < ActiveRecord::Migration
  def change
    create_table :photo_comments do |t|
      t.text :comment
      t.references :user, index: true
      t.string :title
      t.boolean :audit

      t.timestamps
    end
  end
end
