class RemoveImgUrlFromImages < ActiveRecord::Migration
  def change
		remove_column :images, :img_url
		add_column :images, :img, :string
  end
end
