class CreateCategoriesPhotos < ActiveRecord::Migration
  def change
    create_table :categories_photos do |t|
      t.integer :photo_id
      t.integer :category_id
    end
    add_index :categories_photos, :photo_id
    add_index :categories_photos, :category_id
  end
end
