class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :name
      t.boolean :status
      t.integer :cat_type

      t.timestamps
    end
  end
end
