class AddPhotoIdToPhotoComment < ActiveRecord::Migration
  def change
    add_column :photo_comments, :photo_id, :integer
  end
end
