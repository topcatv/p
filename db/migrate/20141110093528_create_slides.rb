class CreateSlides < ActiveRecord::Migration
  def change
    create_table :slides do |t|
      t.references :photo, index: true
      t.integer :type
      t.references :product, index: true

      t.timestamps
    end
  end
end
