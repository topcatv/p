class AddAuditToPhoto < ActiveRecord::Migration
  def change
    add_column :photos, :audit, :boolean
  end
end
