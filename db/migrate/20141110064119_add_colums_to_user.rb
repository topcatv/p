class AddColumsToUser < ActiveRecord::Migration
  def change
    add_column :users, :tel, :string
    add_column :users, :keywords, :string
    add_column :users, :copyright, :string
    add_column :users, :birthday, :date
    add_column :users, :sex, :string
    add_column :users, :blood, :string
    add_column :users, :shoot_start_date, :date
    add_column :users, :equipment, :string
    add_column :users, :qq, :string
    add_column :users, :wangwang, :string
    add_column :users, :website, :string
    add_column :users, :weibo_name, :string
    add_column :users, :weibo_url, :string
    add_column :users, :weixin, :string
    add_column :users, :city_id, :integer
  end
end
