json.array!(@photos) do |photo|
  json.extract! photo, :id, :pic, :name, :description, :price, :location, :shoot_time
  json.url photo_url(photo, format: :json)
end
