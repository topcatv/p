class PhotosController < ApplicationController
	respond_to :html, :json
  before_action :set_photo, only: [:like, :show, :edit, :update, :destroy]

  def index
    @photos = Photo.paginate(page: params[:page], per_page: 1)
    respond_with(@photos)
  end

  def show
		@comments = @photo.comments.order("created_at DESC").paginate(page: params[:page], per_page: 3)
    respond_with(@photo, @comments)
  end

  def new
    @photo = Photo.new
    respond_with(@photo)
  end

  def edit
  end

  def create
    @photo = current_user.photos.build(photo_params)
    @photo.save
    respond_with(@photo)
  end

  def update
    @photo.update(photo_params)
    respond_with(@photo)
  end

  def destroy
    @photo.destroy
    respond_with(@photo)
  end

	def like
		Photo.increment_counter :praise, @photo.id
		@photo.reload
	end

  private
    def set_photo
      @photo = Photo.find(params[:id])
    end

    def photo_params
      params.require(:photo).permit(:pic, :name, :description, :price, :location, :shoot_time, category_ids: [])
    end
end
