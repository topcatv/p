class ProfilesController < ApplicationController
	respond_to :html, :json
	
	def index
		@photos = current_user.photos
		@products = current_user.products

		respond_with(@photos, @products)
	end

	def show
		@user = User.find(params[:id])

		@photos = @user.photos
		@products = @user.products
		
		respond_with(@user, @photos, @products)

	end
end
