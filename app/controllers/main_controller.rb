class MainController < ApplicationController
	respond_to :html, :json

	def index
		@photos = Photo.paginate(page: params[:page], per_page: 2)
		@slides = Slide.all

		respond_with(@photos, @slides)
	end
end
