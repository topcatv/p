class PhotoCommentController < ApplicationController
	before_filter :find_photo

	def new
		@comment = PhotoComment.new

		respond_to do |format|
			format.html # new.html.erb
			format.xml  { render xml: @comment }
		end
	end

	def show
		@comment = PhotoComment.find(params[:id])

		respond_to do |format|
			format.html # show.html.erb
			format.xml  { render xml: @comment }
		end
	end

	def create
		@comment = PhotoComment.new(comment_params)
		@comment.photo_id = @photo.id
		@comment.user_id = current_user.id

		respond_to do |format|
			if @comment.save
				format.js
				format.json {render json: @photo, status: :created}
			else
				format.js
				format.json {render json: @comment.errors, status: :unprocessable_entity}
			end
		end
	end

	protected
	def find_photo 
		@photo = Photo.find(params[:photo_id])
	end

	def comment_params
		params.require(:photo_comment).permit(:comment)
	end

end
