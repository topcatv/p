class ProductsController < ApplicationController
  respond_to :html, :json	
	before_action :set_product, only: [:show, :edit, :update]

	def new
		@product = Product.new
		respond_with(@product)
	end

	def create
		@product = current_user.products.build(product_params)

		respond_to do |format|
			if @product.save
				flash[:notice] = 'Product was successfully created.'
				format.html { redirect_to(@product) }
				format.xml  { render xml: @product, status: :created, location: @product }
			else
				format.html { render action: 'new' }
				format.xml  { render xml: @product.errors, status: :unprocessable_entity }
			end
		end
	end

	def show
		respond_with(@product)
	end

	def edit
	end

  def update
    @product.update(product_params)
    respond_with(@product)
  end

	def destroy
		@product = Product.find(params[:id])
		@product.destroy

		respond_to do |format|
			format.html { redirect_to(profile_index_url) }
			format.xml  { head :ok }
		end
	end

	private

		def set_product
			@product = Product.find(params[:id])
		end

		def product_params
			params.require(:product).permit(:name, :description, :price, images_attributes: [:id, :img, :_destroy])
		end

end
