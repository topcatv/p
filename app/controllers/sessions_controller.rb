class SessionsController < Devise::SessionsController

  def create
    if captcha_valid? params[:captcha]
      super
    else
      build_resource
      flash[:error] = '验证码错误!'
      respond_with_navigational(resource) { render :new }
    end
  end

  # Build a devise resource passing in the session. Useful to move
  # temporary session data to the newly created user.
  def build_resource(hash=nil)
    self.resource = resource_class.new_with_session(hash || {}, session)
  end

end
