class SlidesController < ApplicationController
	layout :admin
	respond_to :html, :json

	def index
		@slides = Slide.all
    @photos = Photo.paginate(page: params[:page], per_page: 1)

		respond_with(@slides, @photos)
	end

	def add_slide
		@slide = Slide.new()
		@slide.photo = Photo.find(params[:id])
		@slide.save

		redirect_to :slides
	end

	def destroy
		@slide = Slide.find(params[:id])
		@slide.destroy

		respond_to do |format|
			format.html { redirect_to(slides_url) }
		end
	end

	private

	def admin
		'admin'
	end
end
