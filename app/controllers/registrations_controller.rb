class RegistrationsController < Devise::RegistrationsController
	protect_from_forgery

  def create
    if captcha_valid? params[:captcha]
      super
    else
      build_resource(sign_up_params)
      clean_up_passwords(resource)
      flash.now[:alert] = '验证码错误!'
      respond_with_navigational(resource) { render :new }
    end
  end

end
