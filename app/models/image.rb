class Image < ActiveRecord::Base
	mount_uploader :img, PhotoUploader
	attr_accessor :uploader_secure_token
end
