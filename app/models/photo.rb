class Photo < ActiveRecord::Base
	belongs_to :user
	has_many :comments, class_name: "PhotoComment"
	has_and_belongs_to_many :categories

	mount_uploader :pic, PhotoUploader
	attr_accessor :uploader_secure_token
end
