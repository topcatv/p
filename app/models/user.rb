class User < ActiveRecord::Base
	has_many :photos
	has_many :products
	belongs_to :city
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
				 #:confirmable, :async
  validates :username, uniqueness: { case_sensitive: false }, length: {in: 5..20}

	mount_uploader :avatar, AvatarUploader
	attr_accessor :uploader_secure_token, :province

  def login=(login)
    @login = login
  end

  def login
    @login || self.username || self.email
  end

	def is_admin?
		admin
	end

  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions).where(['username = :value OR lower(email) = lower(:value)', { value: login }]).first
    else
      where(conditions).first
    end
  end

	def update_with_password(params={})
		if !params[:current_password].blank? or !params[:password].blank? or !params[:password_confirmation].blank?
			super
		else
			params.delete(:current_password)
			self.update_without_password(params)
		end
	end

	def type_choiced?
		!user_type.nil?
	end

end
