module ApplicationHelper

  def fulltitle(title)
    full_title = "云摄影"
    full_title += " | #{title}" unless title.blank?
    full_title
  end

	def show_user_type(user)
		if user.user_type == 1
			"爱秀"
		elsif user.user_type == 2
			"爱摄"
		else
			""
		end
  end

  def user_avatar_tag(user, options = {})
    options[:style] ||= :small
    style = case options[:style].to_s
              when "origin" then ""
              when "small" then "30x30"
              when "normal" then "190x190"
              when "large" then "240x240"
              when "large_w" then "960x"
              when "large_h" then "x960"
              else options[:style].to_s
            end
		if options[:image_options].nil?
			image_tag(user.avatar.url(style))
		else
			image_tag(user.avatar.url(style), options[:image_options])
		end
  end
end
