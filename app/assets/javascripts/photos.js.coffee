# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).on 'ready page:load', ->	
	$('#dt_shoot_time').datetimepicker({
			format: 'yyyy-mm-dd',
			minView: 'month',
			todayBtn: true,
			todayHighlight: true,
			language: 'zh-CN',
			autoclose: true
	})

	$('.chosen-select').chosen
	    allow_single_deselect: true
			no_results_text: '找不到匹配'
			width: '200px'
