# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).on 'ready page:load', ->	
  # 验证码刷新
	$("#captcha_reload").on 'click', ->
    src = $("img[alt='captcha']").attr('src')
    src = src.split('?')[0] + '?i=' + Date.now()
    $("img[alt='captcha']").attr('src', src)
