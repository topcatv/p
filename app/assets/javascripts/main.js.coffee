# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).on('ready page:load', ->
	masonry = $('#masonry')
	masonry.infinitescroll({
		navSelector  : "div.pagination",   #页面分页元素(成功后会被隐藏)
		nextSelector : "div.pagination a:last", #需要点击的下一页链接，和2的html要对应
		itemSelector : ".item"  , #ajax回来之后，每一项的selecter
		animate      : true,      #加载完毕是否采用动态效果
		extraScrollPx: 100,       #向下滚动的像素，必须开启动态效果
		# debug     : true,      #调试的时候，可以打开
		bufferPx     : 5,         #提示语展现的时长，数字越大，展现时间越短
		loading: {
				finishedMsg: '', #当加载失败，或者加载不出内容之后的提示语
				msgText : ''    #加载时的提示语
			}
	}, (newElements) ->
			newElems = $(newElements)
			newElems.imagesLoaded(->
				masonry.masonry('appended', newElems, true)
			)
	)

	masonry.imagesLoaded(->
		masonry.masonry({
			itemSelector : '.item',
			columnWidth : 200,
			isFitWidth: true 
		})
	)
)
