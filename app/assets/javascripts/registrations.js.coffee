# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).on 'ready page:load', ->	
  # 验证码刷新
	$("#captcha_reload").on 'click', ->
    src = $("img[alt='captcha']").attr('src')
    src = src.split('?')[0] + '?i=' + Date.now()
    $("img[alt='captcha']").attr('src', src)
  # 日期控件
	$('#dt_shoot').datetimepicker({
			format: 'yyyy-mm-dd',
			minView: 'month',
			todayBtn: true,
			todayHighlight: true,
			language: 'zh-CN',
			autoclose: true
	})
	$('#dt_birthday').datetimepicker({
			format: 'yyyy-mm-dd',
			minView: 'month',
			todayBtn: true,
			todayHighlight: true,
			language: 'zh-CN',
			autoclose: true
	})
